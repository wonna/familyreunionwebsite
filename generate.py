
import os
import shutil
import json

def getTeam(teams, name):
    for team in teams:
        teamName = team["name"]
        if(teamName == name):
            return teamName
        elif name in team["members"]:
             return teamName
    raise Exception("No team member found with name: " + name)

if os.path.exists("public"):
    shutil.rmtree("public")

os.mkdir("public")



data = json.load(open("data.json"))

events = data["events"]

replacements = dict()

replacements["${EVENT_COUNT}"] = str(len(events))

teams = data["teams"]

scores = dict()

teamHtml = "<ul>"

for team in teams:
    teamHtml += "\n\t<li>" + team["name"]
    teamHtml += "\n\t\t<ul>"
    for name in team["members"]:
        teamHtml +="\n\t\t\t<li>" + name + "</li>"
    teamHtml += "</ul></li>"
    scores[team["name"]] = 0

teamHtml += "\n</ul>"

replacements["${TEAM_LIST}"] = teamHtml


eventsHtml = "<br/>"
for event in events:
    eventsHtml += '<button type="button" class="collapsible">' + event["name"] + '</button>'
    result = event["results"]

    eventsHtml += '<div class="content"><p>'
    eventsHtml += "Results: "
    if result is None:
        eventsHtml += "Not yet complete"
    else:
        firstName = result["1st"]
        secondName = result["2nd"]
        thirdName = result["3rd"]
        eventsHtml += "<ol><li>" + firstName + "</li><li>" + \
                        secondName + "</li><li>" + \
                        thirdName + "</li></ol>"

        first = getTeam(teams, firstName)
        second = getTeam(teams, secondName)
        third = getTeam(teams, thirdName)
        scores[first] = scores[first] + 3
        scores[second] = scores[second] + 2
        scores[third] = scores[third] + 1
    
    if not event["details"] is None:
        eventsHtml += "<br/>Details: " + event["details"]
    
    eventsHtml += '</p></div>\n'

replacements["${EVENT_LIST}"] = eventsHtml

standingsHtml = "<ol>"

for score in sorted(set(scores.values()), reverse = True):
    teamsWithScore = list(filter(lambda n : scores[n] == score, scores.keys()))
    if len(teamsWithScore) > 1:
        standingsHtml += "\n\t<li>" + "Tie" + " with a score of " + str(score) + "<ul>"
        for team in teamsWithScore:
            standingsHtml += "<li>" + team + "</li>"
        standingsHtml += "</ul></li>"
    else:
        standingsHtml += "\n\t<li>" + teamsWithScore[0] + " with a score of " + str(score) + "</li>"


standingsHtml += "</ol>"

replacements["${STANDINGS_LIST}"] = standingsHtml


resources = os.listdir("resources")

for fileName in resources:
    content = open("resources/" + fileName).read()
    

    for key in replacements.keys():
        content = content.replace(key, replacements[key])

    outFile = open("public/" + fileName, 'w')
    outFile.write(content)
    outFile.close()